<?php

namespace App\Http\Controllers\Service\Xendit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TransactionController extends Controller
{

	protected $secretKey;

	function __construct()
	{
		$this->secretKey = env('XENDIT_SECRET_KEY');
	}

    public function createInvoice()
    {
    	$options['secret_api_key'] = $this->secretKey;

    	$xenditClient = new \XenditClient\XenditPHPClient($options);

		$external_id = 'xenintegration_'.date('ymdhis');
		$amount = 250000;
		$payer_email = 'newbalance990@gmail.com';
		$description = 'New balance';

		$response = $xenditClient->createInvoice($external_id, $amount, $payer_email, $description);
		dd($response);
    }
}
