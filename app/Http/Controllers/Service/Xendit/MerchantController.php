<?php

namespace App\Http\Controllers\Service\Xendit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MerchantController extends Controller
{
	protected $secretKey;

	function __construct()
	{
		$this->secret_key = env('XENDIT_SECRET_KEY');
	}

    public function getBalance()
    {
    	$options['secret_api_key'] = $this->secret_key;
    	$xenditClient = new \XenditClient\XenditPHPClient($options);

    	$response = $xenditClient->getBalance();

    	dd($response);
    }

    public function createFva()
    {
    	$options['secret_api_key'] = $this->secret_key;
    	$xenditClient = new \XenditClient\XenditPHPClient($options);
		
		$external_id = 'demoj_1475459775872';
		$bank_code = 'BCA';
		$name = 'Jihan sastro';
    	
    	$response = $xenditClient->createCallbackVirtualAccount($external_id, $bank_code, $name);

    	dd($response);
    }
}
