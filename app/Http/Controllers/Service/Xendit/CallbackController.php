<?php

namespace App\Http\Controllers\Service\Xendit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CallbackController extends Controller
{
	protected $secretKey;

	function __construct()
	{
		$this->secret_key = env('XENDIT_SECRET_KEY');
	}

	public function callbackFvaPaid(Request $request)
	{
		\Log::info("fvaPaid".json_encode($request->all()));
	}

	public function callbackInvoicePaid(Request $request)
	{
		\Log::info("---------------------------------------------------------------------------");
		\Log::info("invoicePaid".json_encode($request->all()));
		\Log::info("invoicePaid:Headers".json_encode($request));
	}
}
